var express = require('express'),
	app = express(),
	port = process.env.PORT || 3001;

var path = require('path');

var requestjson = require('request-json');

var MovimientosJSON = require('./otros_movimientos.json');

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(
	function(req, res, next) {
		res.header("Access-Control-Allow-Origin","*");
		res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
		next();
		}
	);

app.listen(port);

var urlRaizMLab = 'https://api.mlab.com/api/1/databases/jlopezp/collections';
var apiKey = 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var urlClientes = 'https://api.mlab.com/api/1/databases/jlopezp/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';

console.log('todo list RESTful API server started on: ' + port);

//app.get('/', function(req, res){res.send("Hola Mundo");});
app.get('/',
	function(req, res){
		res.sendFile(path.join(__dirname,'index.html'));
		}
	);
app.get('/Clientes/:idCliente', function(req, res){res.send("Aquí tiene el cliente número " + req.params.idCliente);});
app.post('/', function(req, res){res.send("Hemos recibido su petición post");});
app.put('/', function(req, res){res.send("Hemos recibido su petición put cambiada");});
app.delete('/', function(req, res){res.send("Hemos recibido su petición delete");});
app.get('/v1/movimientos', function(req, res){ res.sendfile('movimientos.json'); });
app.get('/v2/movimientos', function(req, res){ res.json(MovimientosJSON); });
app.get('/v2/movimientos/:indice',
	function(req, res){
		console.log(req.params.indice);
		res.send(MovimientosJSON[req.params.indice]);
		}
	);
app.get('/v3/movimientosquery',
	function(req, res){
		console.log(req.query);
		res.send('Recibido');
		}
	);
app.post('/v3/movimientos',
	function(req, res){
		var nuevo = req.body;
		nuevo.id = MovimientosJSON.length + 1;
		MovimientosJSON.push(nuevo);
		res.send('Movimiento dado de alta');
		}
	);


app.get('/Clientes',
  function(req, res) {

    var clienteMLab = requestjson.createClient(urlClientes);
    clienteMLab.get('',
      function(err, resM, body) {
        if(err) {
          console.log(body);
          }
        else {
          res.send(body);
          }
        }
      );
    }
  );

app.post('/login', 
	function(req, res) {
	var clienteMLab = 	
		res.header("Access-Control-Allow-Headers","Content-Type");
		var email = req.body.email;
		var password = req.body.password;
		var query = 'q={"email":"' + email + '", "password":"' + password + '"}';
		clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);
		console.log(urlRaizMLab + "/Usuarios?" + apiKey + "&" + query);

		clienteMLabRaiz.get('', function(err, resM, body) {
			if(!err) {
				if(body.length == 1)
					res.status(200).send('Usuario logueado');
				else
					res.status(404).send('Usuario no encontrado, registrese');
				}
			});
		} 
	);
